// ES6 Updates 

// ES6 is one of the latest version of writing Javascrepit and in cfact is one of the major update to JS 

// let,conts     - are ES6 update are the new standards of creating variables 
// var - was the keyword to create variable before ES6



// console.log(varSample);

// var varSample = "sadasd";


// Exponent operator

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);

//Math.pow() alloe us to get the the result of anumber raiseed to given expont.

// Exponent operator ** - allow us to get the result of a number raised to a given exponent. it is used as an altermative tp Math.pow();
let fivePowerOf4 = 5**2;
console.log(fivePowerOf4);

let squareRootof4 = 4**.5;
console.log(squareRootof4);


let string1 = "Javascript";
let string2 = "not";
let string3 = "is"; 
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";
let string8 = "bootcamp";




// mini activity


    // let sentence1 = string1 + " "+ string3 + " " + string2 + " " + string5 ;
    // let sentence2 = string4 + " " + string3 + " " + string1;

    // console.log(sentence1);
    // console.log(sentence2);



    // Template literals >>>>>>>>>>>>>>>>>>>> is easy to embed the information   
    // "," - string literals
    // Template literaS Allow us to create usinf `` and easily embed JS espression in it
    
    let sentence1 = `${string1} ${string3} ${string2} ${string5} `;////-----> template literal
    let sentence2 = `${string4} ${string3} ${string1}`;

         console.log(sentence1);
         console.log(sentence2);

         /*
            $() is a placeholder it is used to embed  JS espression when creatimg  strimf using Template literals

         */

            // use template literal to add a new sentence in our sentence3 variable:

    let sentence3 = `${string6} ${string7} ${string8}`;
        console.log(sentence3);

        let sentence4  =`The sum of 15 and 25 is ${15+25}`;
        console.log(sentence4);


        // make an object sample for person object

        let person = {
            name: "Micahael ",
            postion: "Developer",
            income:50000,
            expenses:6000
        }

        // console.log(`${person.name} is a ${person.postion}`);
        // console.log(`his income balance is ${person.income} and expenses at ${person.expenses} his current balance is ${person.incom - person.expenses}`
        
        
        // Destrucitionng array and objects
        // Dsstrucuring will allowd us to save array itmes or obk=ject properties into newq varilabe without having create/initialize with acceing then itmes/ properties one by one


        // Array destruciuring

let array1 = ["curry", "lillard","paul", "iriving"];

//array destruciuring is when we save array items into variubles,
// in array, orrder matters and that goes the same to our destructuring
let [player1,player2,player3,player4] = array1;

console.log(player1,player2,player3,player4);

let array2 = ["Jokic","Embiid","Howard","Anthony-Towns"];

// let [center1,center2,center3] = array2;
// console.log(center1,center2,center3);

let [center1,center2,,center4] = array2; //// --> if you want to choose in array by decalring the varible in orderly used by skipiing like double ,, then add new varible center4 for sample 
console.log(center4);



// Object destruciuring
// in the object destruciuring, the order of destruciuring does not matter, however the name of the varible must a property in the object.

let pokemon1 = {
    name:"Bulbasaur",
    type:"Grass",
    level:10,
    moves:["Razor leaf","Tackle","leaf Seed"]
}

let {level,type,name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality); 



let pokemon2 = {
    name:"Charmander",
    type:"fire",
    level:11,
    moves:["Ember","Scratch"]
}

//{Pro}
const {name:name2} =  pokemon2;
console.log(name2);

// let {name2=name} = pokemon2;

// // console.log(moves);

// console.log(name2);

// Arrow dunction
// Arrow function are an alternative way of wringr function in Js
// howeve, there are significant pros ab=nd cons between tradiotiona and arrow


// tradiotional function
function diplayMsg() {

    console.log("heloooe, word");

    
}
diplayMsg();



// Arrow function 
const hello = () =>{
    console.log(`helloe from Arrow!`);
}

hello();

// Arrow function with Paremters 

const greet =(friend)=>{
        console.log(`Hi ${friend.name}`);
}

greet(person);


//Implicit return, we return value without the return keyword 
let added = (num1,num2) => num1 - num2; /// Arrow function without culry brace or one line without {}
let minus = added(10,5);
console.log(minus);



// Arrow vs tradiotional function

// Implicit Return - allows us to return a value from arrow function without the use return keyword 

//  function addNum(num1,num2) {

    // console.log(num1,num2);
    
    // let result = num1 + num2;
    // return result;

//     return num1+num2;
//  }

//  let sum =  addNum(5,10);
//  console.log(sum);

//Implicit return, we return value without the return keyword 
 let subNum = (num1,num2) => num1 - num2; /// Arrow function without culry brace or one line without {}
 let difference = subNum(10,5);
 console.log(difference);


 // implicit reutn will only work arrow witout {}
 //{ in arrow function/funvtion are code block, if an arrow function has {}
 // has a{} or code block, we er going to need tomuse a ret



//  let subNum = (num1,num2) => {return num1 - num2}; // multi line // need return keyword

//  let difference = subNum(10,5);
//  console.log(difference);




// mini activty 

// translate  tradiotional function unto arrow function


let addSum = (num1,num2) => num1 + num2; /// Arrow function without culry brace or one line without {}
 let add = addSum(50,70);
console.log(add);


//tradiotional function Vs Arrow function as Methods

let character1 = {
    name: "Cloud Strife ",
    occupation : "SOLDIER",
    greet: function name() {
        
        // this keyword refers to th curent object where the method is 
        console.log(this);
        console.log(`Hi I am ${this.name}`);
    },

    // in an  arrow funtio as method
    // the this keyword will NOT refers to the curent object, instead it will refer to the global window object
    introduced:() => {
        console.log(this);
    }
}

character1.greet();
character1.introduced();



// Class based object Blueprints




// Contructor function
function Pokemin(name,type,level) {


    this.name = name;
    this.type = type;
    this.level = level;
    
}

// with  the advetnt of ES6,  we are now introduced as special method of craeing and initialize an object
// Class - Pascal
//Normal func - camelCase
class Car{
    constructor(brand,name,year){
        this.brand =brand;
        this.name =name;
        this.year = year;
    }
}


let car1 = new Car("Toyota", "Vios","2002")
let car2 = new Car("Cooper", "Mini","1969")
let car3 = new Car("Porches", "911","1967")


console.log(car1);
console.log(car2);
console.log(car3);



//  Mini activty 

// 
// class Contructor function to make an object  

class Pokemon {
     constructor(name,power,level){
        this.name = name;
        this.power = power;
        this.level = level;
     }
}

let user1 = new Pokemon("pika","eletricity",100)
let user2 = new Pokemon("bulbasour","water", 700);

console.log(user1);
console.log(user2);



