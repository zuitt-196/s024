/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.





	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/


//2. Create a class constructor able to receive 3 arguments
// It should be able to receive 3 strings and a number
// Using the this keyword assign properties:
// username, 
// role, 
// guildName,
// level 





 const introduce = (student, age, Class) =>{
    console.log(`Hi! I am ${student} I am ${age} years old`)
    console.log(`I studdy the following Courses ${Class}`)
  

 }
 introduce("Vhong",23,"Math");
 introduce("June",45,"English");




// function introduce(student){

// 	console.log("Hi! I'm " + student.names + "." + "I am " + student.ages + " years old.")
// 	console.log("I study the following courses: " + student.class)

// }

// introduce(student1);
// introduce(student2);

// arrow functiom with parameters
const getCube = (num) => {
    console.log(num**3);
}

getCube(5,3)
// function getCube(num){

// 	console.log(Math.pow(num,3));

// }

let numArr = [15,16,32,21,21,2]

numArr.forEach(element => console.log(element));


// let numsSquared =(num) => num**2

// let result = numsSquared(5)
let numsSquared = numArr.map (function(num){

	return num ** 2;

  }
)

console.log(numsSquared);

// 2.clas
class Heroes {
    constructor(username,role,guidName,level){
        this.username = username;
        this.role= role;
        this.guidName= guidName;
        this.level= level;

    }
    
}

let  hero1 = new Heroes("Zilong","push","Figther", 10)
let  hero2 = new Heroes("Hareth","Magic","mage", 12)

console.log(hero1)
console.log(hero2)